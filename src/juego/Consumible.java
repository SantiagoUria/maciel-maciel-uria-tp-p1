package juego;

import java.util.Random;
import java.awt.Image;
import entorno.Herramientas;
import entorno.Entorno;

public class Consumible {
    private int x;
    private int y;
    private Image imagen;
    private Random random = new Random();

    public Consumible() {
        this.x = random.nextInt(800 - 65) + 50;
        this.y = random.nextInt(600 - 65) + 50;

        this.imagen = Herramientas.cargarImagen("jeringa.gif");
    }

    public void dibujar(Entorno e) {
        e.dibujarImagen(imagen, x, y, 0, 0.1);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
