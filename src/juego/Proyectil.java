package juego;

import java.awt.Image;
import entorno.Herramientas;
import entorno.Entorno;

public class Proyectil {
    private double x;
    private double y;
    private double ancho;
    private double alto;
    private double angulo;
    private Image imagen;
    private double velocidad;

    public Proyectil(Personaje personaje) {
        this.x = personaje.getX();
        this.y = personaje.getY();
        this.ancho = 6;
        this.alto = 3;
        this.angulo = personaje.getAngulo();
        this.imagen = Herramientas.cargarImagen("proyectil.gif");
        this.velocidad = 3;
    }

    public void dibujar(Entorno e) {
        e.dibujarImagen(this.imagen, this.x, this.y, this.angulo, 0.20);
    }

    public void disparar() {
        this.x = x + velocidad * Math.cos(angulo);
        this.y = y + velocidad * Math.sin(angulo);
    }

    public boolean impacta() {
        if (this.x - this.ancho > 800 || this.x - this.ancho < 0 || this.y - this.alto > 600
                || this.y - this.alto < 0) {
            return true;
        } else {
            return false;
        }
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }
}
