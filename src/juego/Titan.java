package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Titan {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double angulo;
	private double velocidad;
	private Image imagen;

	public Titan(Personaje pj) {
		this.x = (double) ((Math.random() * (700 - 100)) + 100);
		this.y = (double) ((Math.random() * (500 - 100)) + 100);
		this.ancho = 60;
		this.alto = 120;
		this.velocidad = (double) ((Math.random() * (5 - 1)) + 1) * 0.10 + 0.1;
		this.imagen = Herramientas.cargarImagen("./titan-sprite.png");
		while(!this.posicionValida(pj)) {
			this.x = (double) ((Math.random() * (700 - 100)) + 100);
			this.y = (double) ((Math.random() * (500 - 100)) + 100);
		}
	}

	public int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}

	void dibujarTitan(Entorno e) {
		e.dibujarImagen(this.imagen, this.x, this.y, this.angulo, 0.75);
	}

	boolean estaCerca(Titan titanes) {
		if ((this.x > titanes.getX() + 200 || this.x < titanes.getX() - 200)
				|| (this.y > titanes.getY() + 200 || this.y < this.getY() - 200)) {
			return false;
		}
		return true;
	}

	public void seguimiento(Personaje personaje) {
		double diffX = this.x - personaje.getX();
		double diffY = this.y - personaje.getY();
		this.angulo = Math.atan2(diffY, diffX);
		this.x = this.x - velocidad * Math.cos(this.angulo);
		this.y = this.y - velocidad * Math.sin(this.angulo);
	}

	public void colisionar(Obstaculo[] obs) {
		if (estaColisionando(obs, this.x + 5, this.y)) {
			this.x = this.x - getRandomNumber(2, 10);
			this.y = this.y - getRandomNumber(2, 10);
		}
		if (this.estaColisionando(obs, this.x - 5, this.y)) {
			this.x = this.x + getRandomNumber(2, 10);
			this.y = this.y + getRandomNumber(2, 10);
		}
		if (estaColisionando(obs, this.x, this.y + 5)) {
			this.x = this.x - getRandomNumber(2, 10);
			this.y = this.y - getRandomNumber(2, 10);
		}
		if (estaColisionando(obs, this.x, this.y - 5)) {
			this.x = this.x + getRandomNumber(2, 10);
			this.y = this.y + getRandomNumber(2, 10);
		}
	}

	boolean estaColisionando(Obstaculo[] obstaculos, double x, double y) {
		for (Obstaculo obstaculo : obstaculos) {
			if ((x > obstaculo.getX() - (obstaculo.getAncho() / 2) - ancho / 2
					&& x < obstaculo.getX() + (obstaculo.getAncho() / 2) + ancho / 2)
					&& (y > obstaculo.getY() - (obstaculo.getAlto() / 2) - ancho / 2
							&& y < obstaculo.getY() + (obstaculo.getAlto() / 2) + ancho / 2)) {
				return true;
			}
		}
		return false;
	}

	boolean colisionarConDisparo(Proyectil disparo) {
		if (disparo.getX() > this.x - this.ancho / 2 && disparo.getX() < this.x + this.ancho / 2 &&
				disparo.getY() > this.y - this.alto / 2 && disparo.getY() < this.y + this.alto / 2) {
			return true;
		}
		return false;
	}

	boolean colisionarConMikasaTitan(Personaje personaje) {
		if (personaje.isEstaConvertido() == true 
				&& (this.x + (this.ancho/2) >= (personaje.getX() - (personaje.getAncho() /2)) 
				&& this.x - (this.ancho/2)<= (personaje.getX() + (personaje.getAncho() /2))
				&& this.y + (this.alto/2)>= (personaje.getY() - (personaje.getAlto() /2)) 
				&& this.y - (this.alto/2) <= (personaje.getY() + (personaje.getAlto() /2)))) {
			personaje.setEstaConvertido(false);
			return false;
		}
		return true;

	}

	// pregunta si el obstaculo esta en el área de mikasa
	boolean posicionValida(Personaje pj) {
		if ((this.x >= pj.getX() - 150 && this.x <= pj.getX() + 150)
				&& (this.y >= pj.getY() - 150 && this.y <= pj.getY() + 150)) {
			return false;
		}
		return true;
	}

	boolean estaCerca(Obstaculo obs) {
		if (((this.x >= obs.getX() - 150 && this.x <= obs.getX() + 150)
				&& (this.y >= obs.getY() - 150 && this.y <= obs.getY() + 150))) {
			return true;
		}
		return false;
	}

	void redibujarSuperpuestos(Titan[] titan, Personaje pj) {
		for (int i = 0; i < titan.length; i++) {
			for (int j = 0; j < titan.length; j++) {
				while (titan[i].estaCerca(titan[j]) && i != j && (!titan[i].posicionValida(pj)
						&& !titan[j].posicionValida(pj))) {
					titan[i].setX(((Math.random() * (700 - 100)) + 100));
					titan[i].setY(((Math.random() * (500 - 100)) + 100));

				}
			}
		}
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	
	
}
