package juego;

import entorno.Entorno;
import entorno.InterfaceJuego;
import entorno.Herramientas;

import java.awt.Color;
import java.util.Date;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Obstaculo[] obstaculos;
	private Personaje personaje;
	private Titan[] titanes;
	private Proyectil proyectil;
	private Consumible jeringa;

	// Variables de puntaje y control del tiempo
	private final Date createdDate = new java.util.Date();
	private long segundos;
	private int puntos;
	private int titanesMatadosFormaNormal;
	private int titanesMatadosFormaTitan;
	private int consimiblesRecolectados;
	private boolean puedeReaparecerTitan;
	private Date jeringaConsumida;
	private boolean puedeReaparecerConsumible;
	private int temporizadorConsumible;
	private int titanesRestantes;

	// (int) ((Math.random() * (max - min)) + min);

	int cantObstaculos = (int) ((Math.random() * (6 - 4)) + 4);

	int cantTitanes = (int) ((Math.random() * (6 - 4)) + 4);

	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Attack on Titan - Final Season - Grupo 7 - Maciel - Maciel - Uría - V0.01",
				800, 600);

		// crea arrays de obstaculos y titanes, personaje, consumible y proyectil
		this.titanes = new Titan[cantTitanes];
		this.obstaculos = new Obstaculo[cantObstaculos];
		this.jeringa = new Consumible();
		this.personaje = new Personaje();
		this.proyectil = null;
		this.puedeReaparecerTitan = false;
		this.puedeReaparecerConsumible = false;

		// dibuja titanes y obstaculos
		for (int i = 0; i < cantTitanes; i++) {
			titanes[i] = new Titan(personaje);
		}

		for (int i = 0; i < cantObstaculos; i++) {
			obstaculos[i] = new Obstaculo();
		}

		// redibuja los obstaculos y titanes que colisionan entre si
		for (Obstaculo obstaculo : obstaculos) {
			obstaculo.redibujarSuperpuestos(obstaculos);
		}
		for (Titan titan : titanes) {
			titan.redibujarSuperpuestos(titanes, personaje);
		}

		this.consimiblesRecolectados = 0;
		this.titanesMatadosFormaNormal = 0;
		this.titanesMatadosFormaTitan = 0;

		this.titanesRestantes = cantTitanes;

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar
	 * el estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		if (titanesRestantes <= 0 && personaje.getEstaVivo()) {
			this.entorno.dibujarImagen(Herramientas.cargarImagen("win.png"), 400, 300, 0, 1);
			this.entorno.cambiarFont("arial black", 20, Color.WHITE);
			this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 201, 425);
			this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 199, 425);
			this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 426);
			this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 424);
			this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 201, 475);
			this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 199, 475);
			this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 476);
			this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 474);
			this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 201, 525);
			this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 199, 525);
			this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 526);
			this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 524);
			this.entorno.escribirTexto("Puntos totales: " + puntos, 201, 575);
			this.entorno.escribirTexto("Puntos totales: " + puntos, 199, 575);
			this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 576);
			this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 574);
			this.entorno.cambiarFont("arial black", 20, Color.BLACK);
			this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 425);
			this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 475);
			this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 525);
			this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 575);

		} else {

			if (personaje.getEstaVivo()) {
				// dibujar contenido del juego
				this.entorno.dibujarImagen(Herramientas.cargarImagen("FondoJuego1.png"), 400, 300, 0, 1);
				// 300, 0, 1);

				for (int i = 0; i < cantObstaculos; i++) {
					if (obstaculos[i] != null) {
						obstaculos[i].dibujarObstaculos(entorno);
						if (proyectil != null) {
							if (obstaculos[i].colisionarConDisparo(proyectil)) {
								proyectil = null;
							}
						}

					}
				}

				for (int i = 0; i < cantTitanes; i++) {

					if (titanes[i] != null) {
						if (titanes[i] != null) {
							titanes[i].dibujarTitan(entorno);
							titanes[i].seguimiento(personaje);
							titanes[i].colisionar(obstaculos);

						}

						if (proyectil != null) {
							if (titanes[i].colisionarConDisparo(proyectil)) {
								titanes[i] = null;
								proyectil = null;
								titanesRestantes--;
								titanesMatadosFormaNormal++;
							}
							// personaje.colisionaConTitan(titanes);
						}
					} else {
						if (puedeReaparecerTitan && segundos % 5 == 0) {
							titanes[i] = new Titan(personaje);
							titanesRestantes++;
							this.puedeReaparecerTitan = false;
						}
					}

				}

				if (jeringa != null) {
					jeringa.dibujar(entorno);
				}

				personaje.dibujar(entorno);
				personaje.colisionar(obstaculos);
				personaje.colisionaConTitan(titanes);

				// cambia font y crea la interfaz del juego
				this.entorno.cambiarFont("arial black", 20, Color.BLACK);
				this.entorno.escribirTexto("Tiempo: " + segundos, 650, 25);
				this.entorno.escribirTexto("Puntaje: " + puntos, 25, 25);

				// Procesamiento de un instante de tiempo

				// controles del personaje
				if (personaje.estaColisionando(obstaculos, personaje.getX(), personaje.getY()) == false) {
					if (entorno.estaPresionada(entorno.TECLA_ARRIBA) || entorno.estaPresionada('w')) {
						personaje.avanzar();
					}

					if (entorno.estaPresionada(entorno.TECLA_DERECHA) || entorno.estaPresionada('d')) {
						personaje.incrementarAngulo();
					}

					if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) || entorno.estaPresionada('a')) {
						personaje.decrementarAngulo();
					}

					if (entorno.sePresiono(entorno.TECLA_ESPACIO) && this.proyectil == null) {
						this.proyectil = new Proyectil(personaje);
//						Herramientas.play("spark_plug.wav");
					}

					if (this.proyectil != null) {
						this.proyectil.dibujar(entorno);
						this.proyectil.disparar();

						if (proyectil.impacta()) {
							this.proyectil = null;
						}
					}

				}
				// transforma al personaje en un titan
				if (personaje.colisionarConsumible(jeringa)) {
					consimiblesRecolectados++;
					jeringaConsumida = new Date();
					this.jeringa = null;
					personaje.setEstaConvertido(true);
				}

				// controla la colision entre el personaje y los titanes
				for (int i = 0; i < cantTitanes; i++) {
					if (titanes[i] != null && titanes[i].colisionarConMikasaTitan(personaje) == false) {
						titanes[i] = null;
						titanesRestantes--;
						titanesMatadosFormaTitan++;
					}
				}

				// actualiza la cantidad de segundos que se lleva jugando
				this.puntos = (titanesMatadosFormaNormal * 10) + (titanesMatadosFormaTitan * 50)
						+ (consimiblesRecolectados * 100);
				java.util.Date now = new java.util.Date();

				if (this.jeringaConsumida != null) {
					temporizadorConsumible = (int) (Math
							.abs((jeringaConsumida.getTime() / 1000) - (now.getTime() / 1000)));
					if ((this.jeringa == null) && (temporizadorConsumible % 10 == 0) && (temporizadorConsumible > 0)) {
						this.puedeReaparecerConsumible = true;
					}
					if (puedeReaparecerConsumible) {
						this.puedeReaparecerConsumible = false;
						this.jeringa = new Consumible();
					}
				}
				segundos = (int) ((now.getTime() - createdDate.getTime()) / 1000);
				if (segundos % 4 == 0) {
					this.puedeReaparecerTitan = true;
				}

			}

			// si el personaje muere, aparece la pantalla final
			else {
				this.entorno.dibujarImagen(Herramientas.cargarImagen("gameover.png"), 400, 300, 0, 1);
				this.entorno.cambiarFont("arial black", 20, Color.RED);
				this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 201, 425);
				this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 199, 425);
				this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 426);
				this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 424);
				this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 201, 475);
				this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 199, 475);
				this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 476);
				this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 474);
				this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 201, 525);
				this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 199, 525);
				this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 526);
				this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 524);
				this.entorno.escribirTexto("Puntos totales: " + puntos, 201, 575);
				this.entorno.escribirTexto("Puntos totales: " + puntos, 199, 575);
				this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 576);
				this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 574);
				this.entorno.cambiarFont("arial black", 20, Color.BLACK);
				this.entorno.escribirTexto("Titanes eliminados con proyectil: " + titanesMatadosFormaNormal, 200, 425);
				this.entorno.escribirTexto("Titanes eliminados a melee: " + titanesMatadosFormaTitan, 200, 475);
				this.entorno.escribirTexto("Consumibles recolectados: " + consimiblesRecolectados, 200, 525);
				this.entorno.escribirTexto("Puntos totales: " + puntos, 200, 575);
			}

		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
