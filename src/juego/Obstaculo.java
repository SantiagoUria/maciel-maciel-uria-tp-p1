package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Image imagen;

	public Obstaculo() {
		this.x = ((Math.random() * (700 - 100)) + 100);
		this.y = ((Math.random() * (500 - 100)) + 100);
		this.ancho = 50;// hitbox
		this.alto = 50;// hitbox
		this.imagen = Herramientas.cargarImagen("obstaculo.png");
	}

	void dibujarObstaculos(Entorno e) {
		e.dibujarImagen(this.imagen, this.x, this.y, 0, 0.2);
		
	}

	boolean estaCerca(Obstaculo obs) {
		if (((this.x >= obs.getX() - 150 && this.x <= obs.getX() + 150)
				&& (this.y >= obs.getY() - 150 && this.y <= obs.getY() + 150))) {
			return true;
		}
		return false;
	}

	// pregunta si el obstaculo esta en el área de mikasa
	boolean posicionValida() {
		if ((this.x >= 200 && this.x <= 600) && (this.y >= 200 && this.y <= 400)) {
			return false;
		}
		return true;
	}

	void redibujarSuperpuestos(Obstaculo[] obstaculos) {
		for (int i = 0; i < obstaculos.length; i++) {
			for (int j = 0; j < obstaculos.length; j++) {
				while (obstaculos[i].estaCerca(obstaculos[j]) && i != j || (!obstaculos[i].posicionValida()
						&& !obstaculos[j].posicionValida())) {
					obstaculos[i].setX(((Math.random() * (700 - 100)) + 100));
					obstaculos[i].setY(((Math.random() * (500 - 100)) + 100));

				}
			}
		}
	}

	boolean colisionarConDisparo(Proyectil disparo) {
		if (disparo.getX() > this.x - this.ancho / 2 && disparo.getX() < this.x + this.ancho / 2 &&
				disparo.getY() > this.y - this.ancho / 2 && disparo.getY() < this.y + this.ancho / 2) {
			return true;
		}
		return false;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}


}

// si en algun momento queremos hacer que el juego pase de 800 600 a mas,
// tenemos que darle variables a las dimensiones de entorno en juego.java y
// restarlas con 45 y 35. cualquier duda q tomi vea q mierda invento aca y q lo
// explique
