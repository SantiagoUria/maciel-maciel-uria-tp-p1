package juego;

//import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Personaje {
	private double x; // spawn x
	private double y; // spawn y
	private double ancho; // ancho de hitbox?
	private double alto; // alto de hitbox?
	private double angulo;
	private double velocidad;
	private Image imagen;
	private Image imagenTitan;
	private boolean estaConvertido;
	private int tiempo;
	private boolean estaVivo;


	public Personaje() {
		this.x = 400;
		this.y = 300;
		this.ancho = 50;
		this.alto = 50;
		this.velocidad = 1;
		this.angulo = 0;
		this.imagen = Herramientas.cargarImagen("pj-mikasa.png");
		this.imagenTitan = Herramientas.cargarImagen("pj-mikasa-titan.png");
		this.estaConvertido = false;
		this.estaVivo = true;
		this.tiempo = 0;
	}

	public void dibujar(Entorno e) {
		if (estaConvertido) {
			if (tiempo != 0) {
				e.dibujarImagen(this.imagenTitan, this.x, this.y, this.angulo, 0.25);
				tiempo--;
				this.velocidad = 0.5;
				this.alto = 110;
				this.ancho = 70;
			} else {
				estaConvertido = false;
			}
		} else {
			e.dibujarImagen(this.imagen, this.x, this.y, this.angulo, 0.15);
			this.velocidad = 1.5;
			this.ancho = 50;
			this.alto = 50;

		}
	}

	public void incrementarAngulo() {
		angulo += 0.1 * velocidad;
	}

	public void decrementarAngulo() {
		angulo -= 0.1 * velocidad;
	}

	public void avanzar() {
		this.x = x + velocidad * Math.cos(angulo);
		this.y = y + velocidad * Math.sin(angulo);
		colisionarConEntorno();
	}

	boolean estaColisionando(Obstaculo[] obstaculos, double x, double y) {

		for (Obstaculo obstaculo : obstaculos) {
			if ((x > obstaculo.getX() - (obstaculo.getAncho() / 2) - ancho / 2
					&& x < obstaculo.getX() + (obstaculo.getAncho() / 2) + ancho / 2)
					&& (y > obstaculo.getY() - (obstaculo.getAlto() / 2) - alto / 2
							&& y < obstaculo.getY() + (obstaculo.getAlto() / 2) + alto / 2)) {
				return true;
			}
		}
		return false;

	}

	void colisionarConEntorno() {
		if (x < 0 || x > 800 || y < 0 || y > 600) {
			this.x = x - velocidad * Math.cos(angulo);
			this.y = y - velocidad * Math.sin(angulo);
		}
	}

	public void colisionar(Obstaculo[] obs) {
		if (estaColisionando(obs, this.x + 5, this.y)) {
			this.x = this.x - 2;
		}
		if (estaColisionando(obs, this.x - 5, this.y)) {
			this.x = this.x + 2;
		}
		if (estaColisionando(obs, this.x, this.y + 5)) {
			this.y = this.y - 2;
		}
		if (estaColisionando(obs, this.x, this.y - 5)) {
			this.y = this.y + 2;
		}

	}

	public boolean colisionarConsumible(Consumible jeringa) {
		if (jeringa != null) {

			if (this.x >= (jeringa.getX() - 25) && this.x <= (jeringa.getX() + 25) && this.y >= (jeringa.getY() - 25)
					&& this.y <= (jeringa.getY() + 25)) {
				this.tiempo = 900;
				return true;
			}
		}

		return false;
	}

	public void colisionaConTitan(Titan[] titanes) {
		for (Titan titan : titanes) {
			if (titan != null 
					&& (this.x + (this.ancho/2) >= (titan.getX() - (titan.getAncho() /2)) 
					&& this.x - (this.ancho/2)<= (titan.getX() + (titan.getAncho() /2))
					&& this.y + (this.alto/2)>= (titan.getY() - (titan.getAlto() /2)) 
					&& this.y - (this.alto/2) <= (titan.getY() + (titan.getAlto() /2)))) {
				if (this.estaConvertido) {
					titan = null;
				} else {
					this.estaVivo = false;
				}

			}
		}
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setEstaConvertido(boolean estaConvertido) {
		this.estaConvertido = estaConvertido;
	}

	public boolean isEstaConvertido() {
		return estaConvertido;
	}

	public int getTiempo() {
		return tiempo;
	}

	public boolean getEstaVivo() {
		return estaVivo;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	
	
}
